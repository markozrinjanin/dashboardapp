export interface Employee {
  id: number;
  fullname: string;
  email: string;
  hourlyRate: number;
  overtimeHourlyRate: number;
  status: string;
}

export interface Shift {
  id: number;
  employeeId: number;
  clockIn: Date;
  clockOut: Date;
}

export interface Employees {
  employees: Employee[];
}

export interface Shifts {
  shifts: Shift[];
}

export interface RootObject {
  employees: Employee[];
  shifts: Shift[];
}
