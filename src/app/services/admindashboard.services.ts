import { Employee, Employees, Shift, Shifts } from 'src/assets/data/dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdmindashboardService {

  constructor(private httpClient: HttpClient) {}

  /*
   getting all employees
  */
  getEmployees(): Observable<Employees> {
    const employeesUrl = 'http://localhost:3000/employees';
    return this.httpClient.get(employeesUrl).pipe(map(toEmployeesList));
  }

  /*
   getting single employee
   @params id: number
  */
  getEmployee(id: number): Observable<Employee> {
    const employeesUrl = 'http://localhost:3000/employees/' + id;
    return this.httpClient.get(employeesUrl).pipe(map(toEmployeeItem));
  }

  /*
   getting multiple employees
   @params ids: number[]
  */
  getEmployeesById(ids: number[]): Observable<Employees> {
    let employeesUrl = 'http://localhost:3000/employees?';
    ids.forEach((id, index) => {
      if (index < ids.length) {
        employeesUrl += 'id=' + id + '&';
      } else {
        employeesUrl += 'id=' + id;
      }
    });
    return this.httpClient.get(employeesUrl).pipe(map(toEmployeesList));
  }

  /*
   getting all shifts
  */
  getShifts(): Observable<Shifts> {
    const shiftsUrl = 'http://localhost:3000/shifts';
    return this.httpClient.get(shiftsUrl).pipe(map(toShiftsList));
  }

  /*
   getting all shifts for single employee
   @params id: number - employeeId
  */
  getEmployeeShifts(id: number): Observable<Shifts> {
    const shiftsUrl = 'http://localhost:3000/shifts?employeeId=' + id;
    return this.httpClient.get(shiftsUrl).pipe(map(toShiftsList));
  }

  /*
   update employees and shifts
   @params employeesList: Employee[] - list of employees with changes
   @params shiftsList: Shift[] - list of shifts with changes
  */
  updateEmployee(employeesList: Employee[], shiftsList: Shift[]) {
    const listOfRequests = [];
    employeesList.forEach(employee => {
      let employeesUrl = 'http://localhost:3000/employees/' + employee.id;
      const body = employee;
      const update$ = this.httpClient.put<Employee>(employeesUrl, body);
      listOfRequests.push(update$);
    });
    shiftsList.forEach(shift => {
      let shiftUrl = 'http://localhost:3000/shifts/' + shift.id;
      const body = shift;
      const update$ = this.httpClient.put<Shifts>(shiftUrl, body);
      listOfRequests.push(update$);
    });
    const update = forkJoin(listOfRequests).pipe(catchError(error => of(error)));
    update.subscribe();
    return update.subscribe();
  }

}

function toEmployeesList(data): Employees {
  return {'employees': data.map(toEmployeeItem)};
}

function toEmployeeItem(data): Employee {
  return {
    id: data.id,
    fullname: data.fullname,
    email: data.email,
    hourlyRate: data.hourlyRate,
    overtimeHourlyRate: data.overtimeHourlyRate,
    status: data.status
  };
}

function toShiftsList(data): Shifts {
  return {'shifts': data.map(toShiftItem)};
}

function toShiftItem(data): Shift {
  return {
    id: data.id,
    employeeId: data.employeeId,
    clockIn: data.clockIn,
    clockOut: data.clockOut
  };
}
