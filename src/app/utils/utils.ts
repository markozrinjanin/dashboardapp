import { Employee, Employees, Shift, Shifts } from 'src/assets/data/dto';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Utils {
  WORKING_HOURS = 28800000;
  overtimeHours = 0;
  regularTimeHours = 0;

  constructor() {}

  employeeClockedInTime(shifts: Shifts): number {
    let totalTime = 0;
    shifts.shifts.forEach(shift => {
      const time = +new Date(shift.clockOut) - +new Date(shift.clockIn);
      totalTime += time;
    });
    return totalTime;
  }

  getTimePaid(shifts: Shifts) {
    let totalTime = 0;
    let workerOvertime = 0;
    let workerRegularTime = 0;
    shifts.shifts.forEach(shift => {
      const clockedTime = +new Date(shift.clockOut) - +new Date(shift.clockIn);
      totalTime += clockedTime / 1000 / 60 / 60;
      if (new Date(shift.clockIn).getDay() === new Date(shift.clockOut).getDay()) {
        if (clockedTime > this.WORKING_HOURS) {
          workerOvertime += ((clockedTime - this.WORKING_HOURS) / 1000 / 60 / 60);
        }
      }
    });
    workerRegularTime = totalTime - workerOvertime;
    return { overtime: workerOvertime, regularTime: workerRegularTime };
  }

  convertMS(ms) {
    let d;
    let h;
    let m;
    let s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    h += d * 24;
    return h + ' hours ' + m + ' minutes ' + s + ' seconds';
  }

}

