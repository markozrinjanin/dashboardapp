import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { TableMaterialModule } from './table/table.module';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from './table/table.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { EditEmployeesDialogComponent } from './table/edit-employees-dialog/editEmployeesDialog.component';
import { EmployeeDialogComponent } from './table/employee-dialog/employeeDialog.component';
import { FocusableDirective } from './focusable.directive';
import { EditableOnEnterDirective } from './editable/editable-on-enter.directive';
import { ViewModeDirective } from './editable/view-mode.directive';
import { EditableComponent } from './editable/editable.component';
import { EditModeDirective } from './editable/edit-mode.directive';
import {TranslateModule, TranslateLoader, TranslateCompiler } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "/assets/i18n/", ".json");
}
@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    EmployeeDialogComponent,
    EditEmployeesDialogComponent,
    FocusableDirective,
    EditableOnEnterDirective,
    ViewModeDirective,
    EditableComponent,
    EditModeDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TableMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  entryComponents: [TableComponent, EmployeeDialogComponent, EditEmployeesDialogComponent],
  bootstrap: [AppComponent, TableComponent],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ]
})
export class AppModule { }
