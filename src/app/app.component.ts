import { Utils } from './utils/utils';
import { Employees, Shifts } from 'src/assets/data/dto';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AdmindashboardService } from './services/admindashboard.services';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  numberOfEmployees: number;
  employeesList: Employees;
  shiftsList: Shifts;
  overtimePaid = 0;
  regularTimePaid = 0;
  totalTime = 0;
  clockedInTime: string;
  title = 'AdminDashboardApp';

  constructor(private _admindashboardService: AdmindashboardService,
              private _utils: Utils,
              public translate: TranslateService,
              private _cdr: ChangeDetectorRef) {
    translate.use('en');
  }

  ngOnInit(): void {
    this._admindashboardService.getEmployees().subscribe(data => {
      this.employeesList = data;
      this.numberOfEmployees = data.employees.length;
      this.employeesList.employees.forEach(employee => {
        this._admindashboardService.getEmployeeShifts(employee.id).subscribe(shift => {
          const timeDistribution = this._utils.getTimePaid(shift);
          this.overtimePaid += timeDistribution.overtime * employee.overtimeHourlyRate;
          this.regularTimePaid += timeDistribution.regularTime * employee.hourlyRate;
        });
      });
    });

    this._admindashboardService.getShifts().subscribe(data => {
      this.shiftsList = data;
      this.shiftsList.shifts.forEach(shift => {
        this.getClockedInTime(shift.clockIn, shift.clockOut);
      });
      this._cdr.detectChanges();
    });
  }

  getClockedInTime(clockIn: Date, clockOut: Date) {
    const time = +new Date(clockOut) - +new Date(clockIn);
    this.totalTime += time;
    this.clockedInTime = this._utils.convertMS(this.totalTime);
  }
}
