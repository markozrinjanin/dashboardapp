import { Employee, Shifts } from './../../assets/data/dto';
import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Employees } from 'src/assets/data/dto';
import { AdmindashboardService } from '../services/admindashboard.services';
import { Utils } from '../utils/utils';
import { MatDialog } from '@angular/material';
import { EditEmployeesDialogComponent } from './edit-employees-dialog/editEmployeesDialog.component';
import { EmployeeDialogComponent } from './employee-dialog/employeeDialog.component';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'table-pagination',
  styleUrls: ['table.component.scss'],
  templateUrl: 'table.component.html'
})
export class TableComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['select',
                                'fullname',
                                'email',
                                'totalClockedInTime',
                                'totalPaidRegular',
                                'totalPaidOvertime',
                                'view'];
  employeesList: Employees;
  employeesDetails: EmployeeDetails = {
    id: 0,
    fullname: '',
    email: '',
    totalClockedInTime: '',
    totalPaidOvertime: 0,
    totalPaidRegular: 0,
    status: 'false',
    shifts: null,
    hourlyRate: 0,
    overtimeRate: 0
  };
  employeesDetailsList: EmployeeDetails[] = [];
  dataSource;
  checkedEmployees = [];

  constructor(private _admindashboardService: AdmindashboardService,
              private _utils: Utils,
              private _cdr: ChangeDetectorRef,
              public dialog: MatDialog) {
    this.dataSource = [];
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this._admindashboardService.getEmployees().subscribe(data => {
      this.employeesList = data;
      this.employeesList.employees.forEach(employee => {
        const currentEmployee: EmployeeDetails = {
          id: 0,
          fullname: '',
          email: '',
          totalClockedInTime: '',
          totalPaidOvertime: 0,
          totalPaidRegular: 0,
          status: 'false',
          shifts: null,
          hourlyRate: 0,
          overtimeRate: 0
        };
        currentEmployee.id = employee.id;
        currentEmployee.fullname = employee.fullname;
        currentEmployee.email = employee.email;
        currentEmployee.status = employee.status;
        this._admindashboardService.getEmployeeShifts(employee.id).subscribe(shifts => {
          const timeDistribution = this._utils.getTimePaid(shifts);
          currentEmployee.totalClockedInTime = this._utils.convertMS(this._utils.employeeClockedInTime(shifts));
          currentEmployee.totalPaidRegular = timeDistribution.regularTime * employee.hourlyRate;
          currentEmployee.totalPaidOvertime = timeDistribution.overtime * employee.overtimeHourlyRate;
        });
        this.employeesDetailsList.push(currentEmployee);
      });
      this.dataSource =  new MatTableDataSource<EmployeeDetails>(this.employeesDetailsList);
      this._cdr.detectChanges();
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ngAfterViewInit() {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showEmployee(id: number) {
    const selectedEmployee = this._admindashboardService.getEmployee(id);
    let employeeData: Employee;
    selectedEmployee.subscribe(data => {
      employeeData = data;
      this.dialog.open(EmployeeDialogComponent, {
        data: employeeData
      });
    });
  }

  onCheck(evt) {
    if (!this.checkedEmployees.includes(evt)) {
      this.checkedEmployees.push(evt);
    } else {
      const index = this.checkedEmployees.indexOf(evt);
      if (index > -1) {
        this.checkedEmployees.splice(index, 1);
      }
    }
  }

  openEditDialog() {
    if (this.checkedEmployees.length > 0) {
      const checkedEmployeesData = this._admindashboardService.getEmployeesById(this.checkedEmployees);
      const listCheckedEmployes: EmployeeDetails[] = [];
      checkedEmployeesData.subscribe(data => {
        data.employees.forEach(employee => {
          const employeeData: EmployeeDetails = {
            id: 0,
            fullname: '',
            email: '',
            totalClockedInTime: '',
            totalPaidOvertime: 0,
            totalPaidRegular: 0,
            status: '',
            shifts: null,
            hourlyRate: 0,
            overtimeRate: 0
          };
          const employeeShifts = this._admindashboardService.getEmployeeShifts(employee.id);
          employeeShifts.subscribe(shifts => {
            employeeData.id = employee.id;
            employeeData.fullname = employee.fullname;
            employeeData.email = employee.email;
            employeeData.status = employee.status;
            employeeData.hourlyRate = employee.hourlyRate;
            employeeData.overtimeRate = employee.overtimeHourlyRate;
            employeeData.shifts = shifts;
          });
          listCheckedEmployes.push(employeeData);
        });
        setTimeout(() => {
          this.dialog.open(EditEmployeesDialogComponent, {
            data: {data: listCheckedEmployes}
          });
        }, 400);
      });
    } else {
      this.dialog.open(EditEmployeesDialogComponent, {
        data: []
      });
    }
  }
}

export interface EmployeeDetails {
  id: number;
  fullname: string;
  email: string;
  totalClockedInTime: string;
  totalPaidRegular: number;
  totalPaidOvertime: number;
  hourlyRate: number;
  overtimeRate: number;
  status: string;
  shifts: Shifts;
}
