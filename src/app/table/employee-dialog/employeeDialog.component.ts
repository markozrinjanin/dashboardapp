import { Employee } from '../../../assets/data/dto';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'data-dialog',
  templateUrl: 'employeeDialog.component.html',
})
export class EmployeeDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}

export interface DialogData {
  data: Employee;
}
