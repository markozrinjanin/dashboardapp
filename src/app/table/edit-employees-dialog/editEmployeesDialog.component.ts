import { AdmindashboardService } from 'src/app/services/admindashboard.services';
import { Utils } from './../../utils/utils';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { EmployeeDetails } from '../table.component';
import { Employee, Shift } from 'src/assets/data/dto';

@Component({
  selector: 'edit-employees-dialog',
  templateUrl: 'editEmployeesDialog.component.html',
  styleUrls: ['editEmployeesDialog.component.scss']
})
export class EditEmployeesDialogComponent implements OnInit, AfterViewInit {
  controls: FormArray;
  shiftsChanged = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private _utils: Utils,
              private _admindashboardService: AdmindashboardService) {}

  ngAfterViewInit(): void {
    if (this.data.data) {
      const toGroups = this.data.data.map(entity => {
        return new FormGroup({
          id: new FormControl(entity.id),
          fullname: new FormControl(entity.fullname),
          email: new FormControl(entity.email),
          hourlyRate: new FormControl(entity.hourlyRate),
          overtimeRate: new FormControl(entity.overtimeRate),
          status: new FormControl(entity.status.toString()),
          shifts: new FormControl(new FormArray(
            entity.shifts.shifts.map(shift => {
              return new FormGroup({
                id: new FormControl(shift.id),
                clockIn: new FormControl(shift.clockIn),
                clockOut: new FormControl(shift.clockOut),
                employeeId: new FormControl(shift.employeeId)
              });
            })
          ))
        });
      });
      this.controls = new FormArray(toGroups);
    }
  }

  ngOnInit(): void {

  }

  getTimeDate(data: Date) {
    return new Date(data).toLocaleString();
  }

  getTotalTime(clockIn: Date, clockOut: Date) {
    return this._utils.convertMS(+new Date(clockOut) - +new Date(clockIn));
  }

  getControl(index: number, field: string): FormControl {
    return this.controls.at(index).get(field) as FormControl;
  }

  updateField(index: number, field: string) {
    const control = this.getControl(index, field);
    if (control.valid) {
      this.data.data = this.data.data.map((e, i) => {
        if (index === i) {
          return {
            ...e,
            [field]: control.value
          };
        }
        return e;
      });
    }
  }

  getShiftControl(indexEmployee: number, indexShift: number, field: string): FormControl {
    return (this.controls.at(indexEmployee).get('shifts').value as FormArray).at(indexShift).get(field) as FormControl;
  }

  updateShiftField(indexEmployee: number, indexShift: number, field: string) {
    const control = this.getShiftControl(indexEmployee, indexShift, field);
    if (control.valid) {
        this.data.data[indexEmployee].shifts.shifts = this.data.data[indexEmployee].shifts.shifts.map((e, i) => {
        if (indexShift === i) {
          const shift = {
            ...e,
            [field]: control.value
          };
          this.shiftsChanged[shift.id] = shift;
          return shift;
        }
        return e;
      });
    }
  }

  onSubmitChanges() {
    const employeesChangedList: Employee[] = [];
    const shiftsChangedList: Shift[] = [];
    this.controls.value.forEach(element => {
      employeesChangedList.push({
        id: element.id,
        fullname: element.fullname,
        email: element.email,
        hourlyRate: element.hourlyRate,
        overtimeHourlyRate: element.overtimeRate,
        status: element.status.toString()
      });
    });

    this.controls.value.forEach(element => {
      element.shifts.controls.forEach(shift => {
        shiftsChangedList.push({
          id: shift.value.id,
          clockIn: shift.value.clockIn,
          clockOut: shift.value.clockOut,
          employeeId: shift.value.employeeId
         });
      });
    });
    this._admindashboardService.updateEmployee(employeesChangedList, this.shiftsChanged.filter((n) => n));
  }

}

export interface DialogData {
  data: EmployeeDetails[];
}
